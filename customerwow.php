<?php

require 'config.php';

date_default_timezone_set('UTC');


$ratings = [
     '103' => 'Extremely Happy'     ,
     '102' => 'Very Happy'          ,
     '101' => 'Happy'               ,
     '100' => 'Neutral'             ,
    '-101' => 'Unhappy'             ,
    '-102' => 'Very Unhappy'        ,
    '-103' => 'Extremely Unhappy'   ,
];


foreach (get_surveys() as $survey) {
    // get agent
    $agent      = get_freshdesk_data('agents/' . $survey->agent_id);
    $agent_name = $agent->contact->name;
    
    $rating = $survey->ratings->default_question;
    
    if ($rating == '103') {
        $text = 'CUSTOMER WOW!!!!!!!!';
        
        if ($agent_name) {
            $text .= " for $agent_name";
        }
        
    } else {
        $text = "New Freshdesk customer rating! Someone is feeling {$ratings[$rating]}";
        
        if ($agent_name) {
            $text .= " about $agent_name";
        }
    }
    
    send_message_to_slack($text);
    send_message_to_slack("<http://support.lightbulb.digital/helpdesk/tickets/" . $survey->ticket_id . "|View ticket>");
}

function get_surveys() {
    $endpoint           = 'surveys/satisfaction_ratings';
    $five_minutes_ago   = date('Y-m-d\TH:i:s', strtotime('-5 minutes'));
    
    $params = [ 'created_since' => $five_minutes_ago ];
    
    return get_freshdesk_data($endpoint, $params);
}

function get_freshdesk_data($endpoint, $params = []) {
    $base = 'https:// ' .FRESHDESK_DOMAIN . '.freshdesk.com/api/v2/';
    
    $url = $base . $endpoint;
    
    if (count($params)) {
        $url_params = [];
        
        foreach ($params as $k => $v) {
            $url_params[] = "$k=$v";
        }
        
        $url .= '?' . implode('&', $url_params);
    }

    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_HEADER         , true                      );
    curl_setopt($ch, CURLOPT_USERPWD        , FRESHDESK_API_KEY . ':X'  );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER , true                      );
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST , false                     );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER , false                     );

    $server_output  = curl_exec($ch);
    $info           = curl_getinfo($ch);
    $header_size    = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $headers        = substr($server_output, 0, $header_size);
    $response       = substr($server_output, $header_size);

    return json_decode($response);
}

function send_message_to_slack($message) {
    $url = 'https://hooks.slack.com/services/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';
    $data = 'payload={"channel": "#general", "username": "bensionsiebot", "text": "' . $message . '", "icon_emoji": ":bensionsiebert:"}';
    
    $ch = curl_init($url);
    
    curl_setopt($ch, CURLOPT_POST            , 1    );
    curl_setopt($ch, CURLOPT_POSTFIELDS      , $data);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST  , false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER  , false);
    
    curl_exec($ch);
    curl_close($ch);
}